import React from "react";
import Product from "./product";

export default class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        { id: 1, name: "Macbook Air 11" },
        { id: 2, name: "Macbook 2015" },
        { id: 3, name: "Macbook Air 13" }
      ]
    };
  }

  onItemClick(id, event) {
    event.preventDefault();
    console.log(id);
  }

  render() {
    return (
     
        <Product items={this.state.items} onClick={this.onItemClick} />
        
     
    );
  }
}
