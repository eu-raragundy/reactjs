import React from "react";

export default class Content extends React.Component {
  render() {
    return (
      <section>
        <ul>{this.props.children}</ul>
        <span>Task: {this.props.children.length}</span>
      </section>
    );
  }
}