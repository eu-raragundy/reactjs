import React from "react";

export default class Header extends React.Component {
  render() {
    return (
      <header>
        <a href="#pending">pending</a>
        <a href="#done">done</a>
        <a href="#all">all</a>
      </header>
    );
  }
}
