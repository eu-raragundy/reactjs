import React from "react";

var heroesListData = [
  { name: "Superman", power: "fly with underwear" },
  { name: "Batman", power: "belt with gadgets" },
  { name: "Spiderman", power: "Jump like a monkey" },
  { name: "Hulk", power: "Angry with anyone" }
];

export default class Heroes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: []
    };
  }

  componentDidMount() {
    this._fetchData();
  }

  onResetClick(event) {
    event.preventDefault();
    this.setState({ list: [] });
  }

  onFetchClick(event) {
    event.preventDefault();
    this._fetchData();
  }

  _fetchData() {
    setTimeout(() => {
      this.setState({
        list: heroesListData
      });
    }, 2000);
  }

  render() {
    if (!this.state.list.length) {
      return (
        <div>
          No list!
          <br />
          <a href="#fetch" onClick={this.onFetchClick.bind(this)}>
            Fetch
          </a>
        </div>
      );
    }

    return (
      <div>
        <ul>
          {this.state.list.map(function(heroe, index) {
            return (
              <li key={index}>
                {heroe.name} the {heroe.power}!
              </li>
            );
          })}
        </ul>
        <a href="#reset" onClick={this.onResetClick.bind(this)}>
          Reset
        </a>
      </div>
    );
  }
}
