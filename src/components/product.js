import React from "react";
import PropTypes from "prop-types";

export default class Product extends React.Component {
  render() {
    return (
      <ul>
        {this.props.items.map(
          function(item, index) {
            return (
              <li key={index}
                className="item"
                onClick={this.props.onClick.bind(null, item.id)}
              >
                {item.name}
              </li>
            );
          }.bind(this)
        )}
      </ul>
    );
  }
}

// validador de tipos

Product.propTypes = {
  items: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired
};
