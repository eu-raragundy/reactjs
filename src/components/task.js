import React from "react";
import PropTypes from "prop-types";

export default class Task extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      updated: false,
      count: 0
    };
  }

 

  onUpdate() {
    this.setState({ updated: true, count: this.state.count + 1  });
    // console.log('click');
  }

  

  render() {
    
    return (
      // className, establece la propiedad class al elmento
      <li
        className={this.props.done ? "done item" : "item"}
        onClick={this.onUpdate.bind(this)}
      >
        <input
          type="checkbox"
          defaultChecked={this.props.done ? "checked" : ""}
        />
        {this.props.value}
        {this.state.updated ? (
          <small>
            {" "}
            <b>Update</b>{" "}
          </small>
        ) : (
          ""
        )}
        {this.state.count}
      </li>
    );
  }
}

// validador de tipos
Task.propTypes = { value: PropTypes.string.isRequired, done: PropTypes.bool };

// Establece valores por defento en las propiedades
Task.defaultProps = { value: "Unknow chapter", done: false };
