import React from "react";

export default class Tasks extends React.Component {
  render() {
    // this.props.children, contiene una referencia a los componentes creados
    // dentro de si mismo
    return <ul>{this.props.children}</ul>;

  }
}
