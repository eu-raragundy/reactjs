import React from "react";
import ReactDOM from "react-dom";
import Tasks from "./tasks";
import Task from "./task";
import Header from "./header";
import Content from "./content";

export default class Todo extends React.Component {
  componentDidMount() {
    console.log(
      "componentDidMount, se ejecuta una vez renderizado el componente"
    );
    console.log(ReactDOM.findDOMNode(this.refs.one));
  }

  render() {
    return (
      <div>
        <Header />
        <Content>
          <Task ref="one" value="Introduction" done />
          <Task value="Chapter 1 - First component" done />
          <Task value="Chapter 2 - Properties" done />
          <Task />
        </Content>
        <footer>Copyright...</footer>
      </div>
    );
  }
}
