import React from "react";
import ReactDOM from "react-dom";
import Todo from "./components/todo";
import Image from "./components/image";
import Heroes from "./components/heroes";
import MomentWrapper from "./wrappers/moment";
import Cart from "./components/cart";
import "./index.css";

class Hello extends React.Component {
  render() {
    return <div>{"Saludos " + this.props.value}</div>;
  }
}

class Messages extends React.Component {
  render() {
    return (
      <div>
        <h1>Hola React</h1>
        <Hello value="Rudy" />
      </div>
    );
  }
}

class Link extends React.Component {
  onClick(event) {
    event.preventDefault();
    console.log("You clicked me!");
  }

  render() {
    return (
      <a
        href={this.props.url}
        onClick={this.onClick.bind(this)}
        className={this.props.color}
      >
        {this.props.caption}
      </a>
    );
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    };
  }

  componentWillMount() {
    console.log("construccion::componentWillMount");
    //  Se ejecuta solo una vez tanto en el cliente o en el servidor y cuando el proceso de renderizacion haya concluido
  }

  componentDidMount() {
    console.log("construccion::componentDidMount");
    // Se ejecuta solo una vez solo en el cliente, es un buen lugar para, incluir otro framework JAvaScript, Hacer peticiones Ajax de precarga de datos, establecer algun timer 'setTimeout' o 'setInterval'.
  }

  componentWillReceiveProps(next_props) {
    console.log("actualizacion::componentWillReceiveProps");

    //  Se ejecutara cuando reciba nuevas propiedades, no se ejecutara con la primera renderizacion, Recibe como parametros las futuras propiedades con sus nuevos valores y se puede hacer comparaciones.
    this.setState({ loading: true });
    // Si se utiliza el metodo setState dentro de este metodo no se dispara una nueva renderizacion.
    // Este punto se puede utilizar para generar una transicion antes de que el componente vuelva a ejecutar el render.
  }

  componentWillUpdate(next_props, next_state) {
    console.log("actualizacion::componentWillUpdate");

    // Se ejecuta inmediatamente antes de renderizar las nuevas propiedades o atributos de estados
    // Nunca se ejecuta con la primera renderizacion
    // Recibe dos parametros, las nuevsa propiedades y el nuevo estado.
    // Es un lugar para definir una transicion
    // No se deberia utilizar el metodo setState dentro de este metodo, ya que se generaria una referencia ciclica
    // Si se necesita actualizar un atributo de estado en respuesta a un cambio de propiedad se debe usar obligatoriamente 'componentWillReceiveProps'
  }

  shouldComponentUpdate(next_props, nest_state) {
    // return true
    console.log("actualizacion::shouldComponentUpdate");
    // Se ejecuta antes de renderizar las nuevas propiedades o estados, y nunca se ejecutara con la renderizacion inical.
    // Se puede usar para bloquear la renderizacion retirnando un valor faslse.
    // Si se retorna false, el metodo render() es omitido hasta que exista un nuevo cambio de propiedades o estado y por lo tanto componentWillUpdate y componentDidUpdate no seran ejecutados.
    // Por defecto este método siempre devuelve true para evitar errores.
  }

  componentDidUpdate(prev_props, prev_state) {
    console.log("actualizacion::componentDidUpdate");

    // Se ejecuta inmediatamente despues que el componente haya sido renderizado.
    // Nunca se ejecuta con la primera renderizacion
    // Recibe dos parametros, las propiedades anteriores y el estado anterior.
    // Es un buen lugar para operar con el DOM del componente, ya que en este punto estan todos los cambios representados con la nueva renderizacion.
  }

  componentWillUnmount() {
    console.log("destruccion::componentWillUnmount");
    // Es el unico punto de captura de destruccion, y se ejecutara inmediatamente antes de que el componente se desmonte del DOM.
    // Se usa para hacer algun tipo de limpieza, como eliminacion de times, eliminar elementos del DOM creados dentro de 'componentDidMount'.
  }

  render() {
    var url = "http://misite.com";
    var className = this.state.loading ? "loading" : "loaded";
    return (
      <div>
        <Messages />
        <Link url={url} caption="My site" color="red" />
        <Todo />
        <Image
          src="https://tpc.googlesyndication.com/daca_images/simgad/6650316379003172491?w=600&h=314"
          alt="Amazing Image"
        />

        <Heroes />

        <MomentWrapper />
        <br/>
        <MomentWrapper date={new Date("04/10/1980")} format="MMMMM" />
         
         <Cart  />

        <div className={className} />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
