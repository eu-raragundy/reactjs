import React from "react";
import PropTypes from "prop-types";
import Moment from "moment";

export default class MomentWrapper extends React.Component {
  render() {
    var date = Moment(this.props.date).format(this.props.format);
    return <span>{date}</span>;
  }
}

// validador de tipos
MomentWrapper.propTypes = { date: PropTypes.any, format: PropTypes.string };

// Establece valores por defento en las propiedades
MomentWrapper.defaultProps = {
  date: new Date(),
  format: "MMMM Do YYYY, h:mm:ss"
};
